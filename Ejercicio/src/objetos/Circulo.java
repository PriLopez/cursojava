package objetos;

public class Circulo extends Figura {
private float Radio;
	public Circulo() {
		super();
	}

	public Circulo(String pNombre, float pRadio) {
		super(pNombre);
		Radio= pRadio;
		// TODO Auto-generated constructor stub
	}
	public float getRadio() {
		return Radio;
	}

	public void setRadio(float radio) {
		Radio = radio;
	}

	@Override
	public float Calcularperimetro() {
		// TODO Auto-generated method stub
		return(float)Math.PI * 2 * Radio ;
	}

	@Override
	public float Calcularsuperficie() {
		// TODO Auto-generated method stub
		return (float)Math.PI* Radio * Radio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(Radio);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Circulo)) {
			return false;
		}
		Circulo other = (Circulo) obj;
		if (Float.floatToIntBits(Radio) != Float.floatToIntBits(other.Radio)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return super.toString()+  "\nRadio=" + Radio;
	}

}
