package objetos;

public abstract class Figura {
private String nombre;

public Figura(){
	nombre = "sin nombre";
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public Figura(String pNombre) {
	super();
	this.nombre = pNombre;
}
public abstract float Calcularperimetro ();
public abstract float Calcularsuperficie ();

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj) {
		return true;
	}
	if (obj == null) {
		return false;
	}
	if (!(obj instanceof Figura)) {
		return false;
	}
	Figura other = (Figura) obj;
	if (nombre == null) {
		if (other.getNombre() != null) {
			return false;
		}
	} else if (!nombre.equals(other.getNombre() )) {
		return false;
	}
	return true;
}

@Override
public String toString() {
	return "Figura [nombre=" + nombre + "]";
}





}
