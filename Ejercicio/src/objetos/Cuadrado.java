package objetos;

public class Cuadrado extends Figura {
private float Lado;
	public Cuadrado() {
	 super ();
	}

	public Cuadrado(String pNombre, float pLado) {
		super(pNombre);
		Lado = pLado; 
		// TODO Auto-generated constructor stub
	}

	public float getLado() {
		return Lado;
	}

	public void setLado(float lado) {
		Lado = lado;
	}

	@Override
	public float Calcularperimetro() {
		return Lado*4;
	}

	@Override
	public float Calcularsuperficie() {
		// TODO Auto-generated method stub
		return Lado*Lado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(Lado);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Cuadrado)) {
			return false;
		}
		Cuadrado other = (Cuadrado) obj;
		if (Float.floatToIntBits(Lado) != Float.floatToIntBits(other.Lado)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return super.toString()+ "\n Lado=" + Lado ;
	}
	

}
