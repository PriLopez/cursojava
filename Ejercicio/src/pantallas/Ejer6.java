package pantallas;
import java.util.Scanner;
import java.awt.EventQueue;

import javax.swing.JFrame;

public class Ejer6 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);
		System.out.println("Ingresar curso");
		int c= s.nextInt();
		if (c== 0){
			System.out.println("Jardin de infantes");
		}else {
			if (c >= 1 && c<= 6) {
				System.out.println("Primaria");
			} else {
				System.out.println("Secundaria");
			}
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer6 window = new Ejer6();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer6() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
