package pantallas;

import java.awt.EventQueue;
import java.io.StringWriter;
import java.util.Scanner;

import javax.swing.JFrame;

public class Ejer4 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);
		System.out.println("Ingresar curso");
		int  d= s.nextInt();
		if (d== "a"){
			System.out.println("Hijos");
		}else {
			if (d== "b") {
				System.out.println("Padres");
			} else {
				System.out.println("Abuelos");
			}
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer4 window = new Ejer4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejer4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
